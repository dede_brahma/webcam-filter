# webcam-filter
How to make a JavaScript photobooth app that takes images using the camera on your webcam. JavaScript provides a native API for accessing camera in the form of the navigator.getUserMedia method. Since it handles private data this API work only in secure HTTPS connections and always asks for user permission before proceeding.

[Live Demo](https://example-javascript.herokuapp.com/capture)

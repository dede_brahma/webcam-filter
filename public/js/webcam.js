document.addEventListener('DOMContentLoaded', function () {

  // References to all the element we will need.
  var video = document.querySelector('#camera-stream'),
    image = document.querySelector('#snap'),
    start_camera = document.querySelector('#start-camera'),
    controls = document.querySelector('.controls'),
    take_photo_btn = document.querySelector('#take-photo'),
    delete_photo_btn = document.querySelector('#delete-photo'),
    download_photo_btn = document.querySelector('#download-photo'),
    error_message = document.querySelector('#error-message');
    filters = [{
      name: "Bright",
      filter: "brightness(300%)"
    },{
      name: "Blur",
      filter: "blur(3px)"
    }, {
      name: "Hue",
      filter: "hue-rotate(90deg)"
    }, {
      name: "Saturate",
      filter: "saturate(800%)"
    }, {
      name: "Contrast",
      filter: "contrast(500%)"
    }, {
      name: "Invert",
      filter: "invert(100%)"
    }, {
      name: "Sepia",
      filter: "sepia(400%)"
    }, {
      name: "BnW",
      filter: "grayscale(100%)"
    }, {
      name: "Reset",
      filter: "",
    }];


  // The getUserMedia interface is used for handling camera input.
  // Some browsers need a prefix so here we're covering all the options
  navigator.getMedia = (navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);


  if (!navigator.getMedia) {
    displayErrorMessage("Your browser doesn't have support for the navigator.getUserMedia interface.");
  }
  else {

    // Request the camera.
    navigator.getMedia(
      {
        video: true
      },
      // Success Callback
      function (stream) {

        // Create an object URL for the video stream and
        // set it as src of our HTLM video element.
        video.src = window.URL.createObjectURL(stream);

        // Play the video element to start the stream.
        video.play();
        video.onplay = function () {
          showVideo();
        };

      },
      // Error Callback
      function (err) {
        displayErrorMessage("There was an error with accessing the camera stream: " + err.name, err);
      }
    );

  }



  // Mobile browsers cannot play video without user input,
  // so here we're using a button to start it manually.
  start_camera.addEventListener("click", function (e) {

    e.preventDefault();

    // Start video playback manually.
    video.play();
    showVideo();

  });


  take_photo_btn.addEventListener("click", function (e) {

    e.preventDefault();

    var snap = takeSnapshot();

    // Show image. 
    image.setAttribute('src', snap);
    image.classList.add("visible");

    // Enable delete and save buttons
    delete_photo_btn.classList.remove("disabled");
    download_photo_btn.classList.remove("disabled");

    // Set the href attribute of the download button to the snap url.
    download_photo_btn.href = snap;

    // Pause video playback of stream.
    video.pause();

  });


  delete_photo_btn.addEventListener("click", function (e) {

    e.preventDefault();

    // Hide image.
    image.setAttribute('src', "");
    image.classList.remove("visible");

    // Disable delete and save buttons
    delete_photo_btn.classList.add("disabled");
    download_photo_btn.classList.add("disabled");

    // Resume playback of stream.
    video.play();

  });



  function showVideo() {
    // Display the video stream and the controls.

    hideUI();
    video.classList.add("visible");
    controls.classList.add("visible");
  }


  function takeSnapshot() {
    // Here we're using a trick that involves a hidden canvas element.  

    var hidden_canvas = document.querySelector('canvas'),
      context = hidden_canvas.getContext('2d');

    var width = video.videoWidth,
      height = video.videoHeight;

    var filter = video.style.filter,
        webkitFilter = video.style.webkitFilter;

    if (width && height) {

      // Setup a canvas with the same dimensions as the video.
      hidden_canvas.width = width;
      hidden_canvas.height = height;
      hidden_canvas.filter = filter;
      hidden_canvas.webkitFilter = webkitFilter;

      // Make a copy of the current frame in the video on the canvas.
      context.drawImage(video, width, height, filter, webkitFilter);

      // Turn the canvas image into a dataURL that can be used as a src for our photo.
      return hidden_canvas.toDataURL('image/png');
    }
  }


  function displayErrorMessage(error_msg, error) {
    error = error || "";
    if (error) {
      console.error(error);
    }

    error_message.innerText = error_msg;

    hideUI();
    error_message.classList.add("visible");
  }


  function hideUI() {
    // Helper function for clearing the app UI.

    controls.classList.remove("visible");
    start_camera.classList.remove("visible");
    video.classList.remove("visible");
    snap.classList.remove("visible");
    error_message.classList.remove("visible");
  }

  function findFilterByName(filterArray, name) {
      for (var i = 0; i < filterArray.length; i++) {
        if (filterArray[i].name === name) {
          return filterArray[i];
        }
      }
      // Not found
      return null;
  }
    thisBrowserSupportsCssFilters = function () {
      var prefixes = " -webkit- -moz- -o- -ms- ".split(" ");
      var el = document.createElement('div');
      el.style.cssText = prefixes.join('filter:blur(2px); ');
      return !!el.style.length && ((document.documentMode === undefined || document.documentMode > 9));
    };

  if (thisBrowserSupportsCssFilters()) {
      var buttonsDiv = document.getElementById("filterButtons");

      filters.forEach(function (item) {
        var button = document.createElement("button");
        button.id = item.name;
        button.innerHTML = item.name;
        // This will cause a re-flow of the page but I don't care
        buttonsDiv.appendChild(button);
      });

      function filterClicked(event) {
        event = event || window.event;
        var target = event.target || event.srcElement;
        if (target.nodeName === "BUTTON") {
          var filter = findFilterByName(filters, target.id);
          if (filter) {
            video.style.filter = filter.filter;
            video.style.webkitFilter = filter.filter;
          }
        }
      };
      buttonsDiv.addEventListener("click", filterClicked, false);
    }

});

